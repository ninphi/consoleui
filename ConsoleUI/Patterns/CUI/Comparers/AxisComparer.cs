﻿using System.Collections.Generic;

namespace Patterns.CUI.Comparers
{
    public class AxisComparer : IComparer<int[]>
    {
        public int Compare(int[] a, int[] b)
        {
            if (a[1] > b[1]) return 1;
            else if (a[1] < b[1]) return -1;
            else
            {
                if (a[0] > b[0]) return 1;
                else if (a[0] < b[0]) return -1;
                else return 0;
            }
        }
    }
}