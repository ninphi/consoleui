﻿using System;
using System.Collections.Generic;
using Patterns.CUI;

namespace Patterns.CUI
{
    public class ConsoleUI
    {
        private readonly List<Page> _pages;
        private Page currentPage;

        public ConsoleUI(List<Page> pages, bool cursorVisible = false)
        {
            _pages = pages;

            Console.CursorVisible = cursorVisible;
            currentPage = _pages[0];
        }

        public void Update()
        {
            currentPage.Render();
            Listen();
        }

        public void Listen()
        {
            ConsoleKey key;

            do
            {
                key = Console.ReadKey().Key;
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        currentPage.PrevElement();
                        Update();
                        break;
                    case ConsoleKey.DownArrow:
                        currentPage.NextElement();
                        Update();
                        break;
                    case ConsoleKey.Enter:
                        currentPage.Select();
                        Update();
                        break;
                    default:
                        break;
                }
            } while (key != ConsoleKey.X);
        }
    }
}