﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Enums;
using Patterns.Interfaces;

namespace Patterns.CUI
{
    public abstract class Element : IRenderable, ILinear, IInteractable
    {
        public Action OnSelected { get; }
        public int PX { get; set; }
        public int PY { get; set; }
        public Axis Direction { get; set; }
        public ConsoleColor BGColor { get; set; }
        public ConsoleColor FGColor { get; set; }
        public abstract void Render();

        public Element(Action onSelected)
        {
            OnSelected = onSelected;
        }
    }
}
