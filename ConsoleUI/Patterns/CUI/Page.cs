﻿using System;
using System.Linq;
using System.Collections.Generic;
using Patterns.Interfaces;
using Patterns.CUI.Comparers;

namespace Patterns.CUI
{
    public class Page : IRenderable, IInteractable
    {
        private int currentElement;
        private List<Element> Elements { get; set; }
        public Action OnSelected { get; }
        public ConsoleColor BGColor { get; set; }
        public ConsoleColor FGColor { get; set; }

        public Page()
        {
            Elements = new List<Element>();
            ResetPage();
        }

        public Page(List<Element> elements,
            ConsoleColor bgColor = ConsoleColor.Black,
            ConsoleColor fgColor = ConsoleColor.White)
        {
            Elements = elements;
            BGColor = bgColor;
            FGColor = fgColor;
        }

        public void Render()
        {
            foreach (var e in Elements)
            {
                if (e == Elements[currentElement])
                {
                    e.BGColor = ConsoleColor.Yellow;
                    e.FGColor = ConsoleColor.Black;
                    e.Render();
                }
                else
                {
                    e.BGColor = ConsoleColor.Black;
                    e.FGColor = ConsoleColor.White;
                    e.Render();
                }
            }
        }

        public Page Add<T>(T element) where T : Element
        {
            Elements.Add(element);
            ResetPage();
            return this;
        }

        public void Select()
        {
            Elements[currentElement].OnSelected.Invoke();
        }
        public void NextElement()
        {
            if (currentElement + 2 <= Elements.Count) currentElement++;
        }
        public void PrevElement()
        {
            if (currentElement - 1 >= 0) currentElement--;
        }
        public void ResetElement() => currentElement = 0;
        public void ResetPage()
        {
            Elements = Elements.OrderBy(e => new int[] { e.PX, e.PY }, new AxisComparer()).ToList();
            ResetElement();
        }
    }
}