﻿using System;
using Patterns.Enums;
using Patterns.Interfaces;

namespace Patterns.CUI
{
    public class TextElement : Element, IInteractable
    {
        public string Text { get; }

        public TextElement(int px, int py, string text, Action onSelected,
            ConsoleColor bgColor = ConsoleColor.Black,
            ConsoleColor fgColor = ConsoleColor.White,
            Axis direction = Axis.X) : base(onSelected)
        {
            PX = px;
            PY = py;
            Text = text;
            BGColor = bgColor;
            FGColor = fgColor;

            if (Enum.IsDefined(typeof(Axis), direction))
                Direction = direction;
            else
                Direction = Axis.X;
        }

        public override void Render()
        {
            int x = PX, y = PY;
            Console.BackgroundColor = BGColor;
            Console.ForegroundColor = FGColor;

            foreach (var c in Text)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(c);

                switch (Direction)
                {
                    case Axis.X:
                        x++;
                        break;
                    case Axis.Y:
                        y++;
                        break;
                    case Axis.Both:
                        x++;
                        y++;
                        break;
                    default:
                        break;
                }
            }

            Console.ResetColor();
        }
    }
}