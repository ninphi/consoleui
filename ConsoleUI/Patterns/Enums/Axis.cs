﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Enums
{
    public enum Axis
    {
        Both = 0,
        X = 1,
        Y = 2
    }
}
