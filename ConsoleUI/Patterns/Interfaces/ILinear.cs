﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Enums;

namespace Patterns.Interfaces
{
    public interface ILinear
    {
        int PX { get; set; }
        int PY { get; set; }
        Axis Direction { get; set; }
    }
}
