﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Enums;

namespace Patterns.Interfaces
{
    public interface IRenderable
    {
        ConsoleColor BGColor { get; set; }
        ConsoleColor FGColor { get; set; }
        void Render();
    }
}
