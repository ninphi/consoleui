﻿using System;
using System.Collections.Generic;
using Patterns.CUI;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            var page1 = new Page().Add(new TextElement(10, 5, "Good night Felix, sleep well <3", () => Console.Clear()))
                .Add(new TextElement(3, 4, "Love you Felix.", () => Console.WriteLine("Meow-meow!")))
                .Add(new TextElement(20, 4, "Absolutely adore!", () => Console.Beep()))
                .Add(new TextElement(15, 6, "I'm so happy we have such a lovely cat x3", () => Console.SetCursorPosition(0, 0)));

            var cui = new ConsoleUI(new List<Page> { page1 });

            cui.Update();
        }
    }
}